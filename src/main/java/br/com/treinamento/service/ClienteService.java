package br.com.treinamento.service;

import br.com.treinamento.model.Cliente;
import br.com.treinamento.model.dto.ClienteDTO;

import java.util.List;

public interface ClienteService {

    Cliente create(ClienteDTO clienteDTO);
    ClienteDTO update(Long id, Cliente cliente);
    ClienteDTO findById(Long id);
    Cliente findByCpf(String cpf);
    List<ClienteDTO> findAll();
    void delete(Long id);
    ClienteDTO activeToggle(Long id);
}

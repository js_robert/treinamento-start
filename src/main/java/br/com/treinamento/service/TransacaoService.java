package br.com.treinamento.service;

import br.com.treinamento.model.Conta;

public interface TransacaoService {

    void transferir(Long contaOrigemId, Long contaDestinoId, double valor);
    void saque(Conta conta, double valor);
    void deposito(Long id, double valor);
}

package br.com.treinamento.service;

import java.util.List;

import br.com.treinamento.model.Cliente;
import br.com.treinamento.model.Conta;
import br.com.treinamento.model.dto.ContaDTO;

public interface ContaService {

	Conta create(ContaDTO contaDTO);
	List<ContaDTO> findAll();
	ContaDTO findById(Long id);
	Conta findByAgenciaAndNumeroConta(String agencia, String numeroConta);
	List<Conta> findByCliente(Cliente cliente);
}

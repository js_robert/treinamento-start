package br.com.treinamento.service.impl;

import br.com.treinamento.model.Conta;
import br.com.treinamento.repository.ContaRepository;
import br.com.treinamento.service.TransacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TransacaoServiceImpl implements TransacaoService {

    @Autowired
    private ContaRepository contaRepository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void transferir(Long contaOrigemId, Long contaDestinoId, double valor) {
        Conta contaOrigem = contaRepository.findById(contaOrigemId).get();
        Conta contaDestino= contaRepository.findById(contaDestinoId).get();

        contaOrigem.setSaldo(contaOrigem.getSaldo() - valor);
        contaDestino.setSaldo(contaDestino.getSaldo() + valor);

        contaRepository.save(contaOrigem);
        contaRepository.save(contaDestino);
    }

    @Override
    public void saque(Conta conta, double valor) {
        Conta contaSalva = contaRepository.findById(conta.getId()).get();
        contaSalva.setSaldo(contaSalva.getSaldo() - valor);
        contaRepository.save(contaSalva);
    }

    @Override
    public void deposito(Long id, double valor) {
        Conta conta = contaRepository.findById(id).get();
        conta.setSaldo(conta.getSaldo() + valor);
        contaRepository.save(conta);
    }
}

package br.com.treinamento.service.impl;

import br.com.treinamento.model.Cliente;
import br.com.treinamento.model.dto.ClienteDTO;
import br.com.treinamento.repository.ClienteRepository;
import br.com.treinamento.service.ClienteService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public Cliente create(ClienteDTO clienteDTO) {
        Cliente cliente = mapper.map(clienteDTO, Cliente.class);
        cliente.setAtivo(true);
        return clienteRepository.save(cliente);
    }

    @Override
    public ClienteDTO update(Long id, Cliente cliente) {
        Cliente clienteSalvo = clienteRepository.findById(id).get();
        BeanUtils.copyProperties(cliente, clienteSalvo, "id");
        return mapper.map(clienteRepository.save(clienteSalvo), ClienteDTO.class);
    }

    @Override
    public ClienteDTO findById(Long id) {
        Cliente cliente = clienteRepository.findById(id).get();
        return mapper.map(cliente, ClienteDTO.class);
    }

    @Override
    public List<ClienteDTO> findAll() {
        List<Cliente> clientes = clienteRepository.findAll();
        return clientes.stream()
                .map(cliente -> mapper.map(cliente, ClienteDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public void delete(Long id) {
        clienteRepository.deleteById(id);
        log.info("Delete Successfully");
    }

    @Override
    public ClienteDTO activeToggle(Long id) {
        Cliente cliente = clienteRepository.findById(id).get();
        cliente.setAtivo(!cliente.isAtivo());
        clienteRepository.save(cliente);
        return mapper.map(cliente, ClienteDTO.class);
    }

    @Override
    public Cliente findByCpf(String cpf) {
        return clienteRepository.findByCpf(cpf);
    }
}

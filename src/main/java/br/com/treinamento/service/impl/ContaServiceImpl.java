package br.com.treinamento.service.impl;

import br.com.treinamento.model.Cliente;
import br.com.treinamento.model.Conta;
import br.com.treinamento.model.dto.ContaDTO;
import br.com.treinamento.repository.ContaRepository;
import br.com.treinamento.service.ContaService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ContaServiceImpl implements ContaService {

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private ContaRepository contaRepository;

    @Override
    public Conta create(ContaDTO contaDTO) {
        Conta conta = mapper.map(contaDTO, Conta.class);
        return contaRepository.save(conta);
    }

    @Override
    public List<ContaDTO> findAll() {
        List<Conta> contas = contaRepository.findAll();
        return contas.stream()
                .map(conta -> mapper.map(conta, ContaDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public ContaDTO findById(Long id) {
        Conta conta = contaRepository.findById(id).get();
        return mapper.map(conta, ContaDTO.class);
    }

    @Override
    public Conta findByAgenciaAndNumeroConta(String agencia, String numeroConta) {
        return contaRepository.findByAgenciaAndNumeroConta(agencia, numeroConta);
    }

    @Override
    public List<Conta> findByCliente(Cliente cliente) {
        List<Conta> contas = new ArrayList<>();
        List<Conta> contaList = contaRepository.findByCliente(cliente);
        contaList.forEach(conta -> {
            if (conta != null) {
                contas.add(conta);
            }
        });
        return contas;
    }


}

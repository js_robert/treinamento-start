package br.com.treinamento.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.model.Cliente;
import br.com.treinamento.model.Conta;
import br.com.treinamento.model.dto.ContaDTO;
import br.com.treinamento.service.ClienteService;
import br.com.treinamento.service.ContaService;

@RestController
@RequestMapping(path = "/contas")
public class ContaResource {

	@Autowired
	private ContaService contaService;
	
	@Autowired
	private ClienteService clienteService;
	
	@PostMapping
	public ResponseEntity<Conta> create(@RequestBody ContaDTO contaDTO){
		Conta conta = contaService.create(contaDTO);
		return new ResponseEntity<>(conta, HttpStatus.CREATED);
	}
	
	@GetMapping
	public ResponseEntity<List<ContaDTO>> findAll(){
		return ResponseEntity.ok().body(contaService.findAll());
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<ContaDTO> findById(@PathVariable(name = "id") Long id){
		return ResponseEntity.ok().body(contaService.findById(id));
	}
	
	@GetMapping(value = "/busca-por-cpf/{cpf}")
	public ResponseEntity<List<Conta>> findByCliente(@PathVariable String cpf){
		Cliente cliente = clienteService.findByCpf(cpf);
		List<Conta> contas = contaService.findByCliente(cliente);
		return ResponseEntity.ok().body(contas);
	}

	@GetMapping(value = "/{agencia}/{numero}")
	public ResponseEntity<Conta> findByAgenciaAndNumeroConta(@PathVariable(name = "agencia") String agencia,
															 @PathVariable(name = "numero") String numeroConta) {
		Conta conta = contaService.findByAgenciaAndNumeroConta(agencia, numeroConta);
		return ResponseEntity.ok().body(conta);
	}
}

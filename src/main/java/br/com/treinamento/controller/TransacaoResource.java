package br.com.treinamento.controller;

import br.com.treinamento.model.Conta;
import br.com.treinamento.model.dto.SaqueDepositoDTO;
import br.com.treinamento.model.dto.TransferenciaDTO;
import br.com.treinamento.service.ContaService;
import br.com.treinamento.service.TransacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/transacoes")
public class TransacaoResource {

    @Autowired
    private TransacaoService transacaoService;

    @Autowired
    private ContaService contaService;

    @PostMapping(value = "/transferencia")
    public ResponseEntity<String> tranferencia(@RequestBody TransferenciaDTO transferenciaDTO) {
        Conta contaOrigem = contaService.findByAgenciaAndNumeroConta(transferenciaDTO.getAgenciaOrigem(),
                                                                     transferenciaDTO.getNumeroContaOrigem());
        Conta contaDestino = contaService.findByAgenciaAndNumeroConta(transferenciaDTO.getAgenciaDestino(),
                                                                      transferenciaDTO.getNumeroContaDestino());
        transacaoService.transferir(contaOrigem.getId(), contaDestino.getId(), transferenciaDTO.getValor());
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("Transferência Realizada com Sucesso!");
    }

    @PostMapping(value = "/deposito")
    public ResponseEntity<String> deposito(@RequestBody SaqueDepositoDTO saqueDepositoTO) {
        Conta conta = contaService.findByAgenciaAndNumeroConta(saqueDepositoTO.getAgencia(), saqueDepositoTO.getNumeroConta());
        transacaoService.deposito(conta.getId(), saqueDepositoTO.getValor());
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("Deposito Realizado com Sucesso!");
    }

    @PostMapping(value = "/saque")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<String> saque(@RequestBody SaqueDepositoDTO saqueDepositoTO) {
        Conta conta = contaService.findByAgenciaAndNumeroConta(saqueDepositoTO.getAgencia(), saqueDepositoTO.getNumeroConta());
        transacaoService.saque(conta, saqueDepositoTO.getValor());
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("Saque Realizado com Sucesso!");
    }
}

package br.com.treinamento.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaqueDepositoDTO implements Serializable {

    private String agencia;
    private String numeroConta;
    private double valor;
}

package br.com.treinamento.model.dto;

import java.io.Serializable;
import br.com.treinamento.model.Cliente;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ContaDTO implements Serializable{

	private static final long serialVersionUID = -2066963459631449812L;

	private Long id;
	private String agencia;
	private String numeroConta;
	private Cliente cliente;
	private double saldo;
}

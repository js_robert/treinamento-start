package br.com.treinamento.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClienteDTO implements Serializable {

	private static final long serialVersionUID = 1638144261089466984L;
	
	private Long id;
    private String nome;
    private String cpf;
    private String email;
    private boolean ativo;
}

package br.com.treinamento.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransferenciaDTO implements Serializable {

    private String agenciaOrigem;
    private String numeroContaOrigem;
    private String agenciaDestino;
    private String numeroContaDestino;
    private double valor;
}

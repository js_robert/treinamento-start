package br.com.treinamento.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.treinamento.model.Cliente;
import br.com.treinamento.model.Conta;

@Repository
public interface ContaRepository extends JpaRepository<Conta, Long>{
	
	Conta findByAgenciaAndNumeroConta(String agencia, String numeroConta);
	List<Conta> findByCliente(Cliente cliente);
}
